package com.example.telusko;

// import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentService {

    @Autowired
    StudentRepo repo;

    public List<Student> getStudents() {
        return repo.findAll();
    }

    public void addStudent(Student student) {
        repo.save(student);
    }
}
